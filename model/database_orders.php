<?php

require_once 'database_init.php';

class DBorders {

    public static function getAll() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM orders");
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function delete($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("DELETE FROM orders WHERE id = :id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM orders 
            WHERE id =:id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetch();
    }

    public static function insert($customer, $amount) {
        $db = DBInit::getInstance();
        
        $done = "0";
        
        $statement = $db->prepare("INSERT INTO orders (costumer_id, amount, date, done)
            VALUES (:costumer_id, :amount, NOW(), :done)");
        $statement->bindParam(":costumer_id", $customer);
        $statement->bindParam(":amount", $amount);
        $statement->bindParam(":done", $done);
        $statement->execute();
    }
    
    public static function insertOrderItem($order_id, $item_id, $volume) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("INSERT INTO item_orders (order_id, item_id, volume)
            VALUES (:order_id, :item_id, :volume)");
        $statement->bindParam(":order_id", $order_id);
        $statement->bindParam(":item_id", $item_id);
        $statement->bindParam(":volume", $volume);
        $statement->execute();
    }
    
    public static function update($id, $done, $canceled) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("UPDATE orders SET done= :done, canceled = :canceled WHERE id =:id");
        $statement->bindParam(":done", $done);
        $statement->bindParam(":canceled", $canceled);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function getId() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT MAX(id) FROM orders");
        $statement->execute();
        
        return $statement->fetch();
    }
    
    public static function getAllItems($order_id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM item_orders WHERE order_id =:order_id");
        $statement->bindParam(":order_id", $order_id);
        $statement->execute();

        return $statement->fetchAll();
    }
}

