<?php

require_once 'database_init.php';

class DBUsers {

    public static function getAll() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM users");
        $statement->execute();

        return $statement->fetchAll();
    }
    
     public static function getAllSellers() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM users WHERE type = 'prodajalec'");
        $statement->execute();

        return $statement->fetchAll();
    }
    
    public static function getAllCustomers() {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM users WHERE type = 'stranka'");
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function delete($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("DELETE FROM users WHERE id = :id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM users 
            WHERE id =:id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetch();
    }

    public static function insert($name, $lastname, $username, $email, $password, $type) {
        $db = DBInit::getInstance();
        
        $pass = password_hash($password, PASSWORD_DEFAULT);
        
        $statement = $db->prepare("INSERT INTO users (name, lastname, username, email, password, type)
            VALUES (:name, :lastname, :username, :email, :password, :type)");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":lastname", $lastname);
        $statement->bindParam(":username", $username);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":password", $pass);
        $statement->bindParam(":type", $type);
        $statement->execute();
    }
    
    public static function insertCustomer($name, $lastname, $username, $email, $password, $type, $phone, $adress) {
        $db = DBInit::getInstance();
        
        $pass = password_hash($password, PASSWORD_DEFAULT);
        
        $statement = $db->prepare("INSERT INTO users (name, lastname, username, email, password, type, phone, adress)
            VALUES (:name, :lastname, :username, :email, :password, :type, :phone, :adress )");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":lastname", $lastname);
        $statement->bindParam(":username", $username);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":phone", $phone);
        $statement->bindParam(":adress", $adress);
        $statement->bindParam(":password", $pass);
        $statement->bindParam(":type", $type);
        $statement->execute();
    }

    public static function update($id, $name, $lastname, $email, $password) {
        $db = DBInit::getInstance();

        $pass = password_hash($password, PASSWORD_DEFAULT);
        
        $statement = $db->prepare("UPDATE users SET name = :name, lastname = :lastname,
            email = :email, password = :password WHERE id =:id");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":lastname", $lastname);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":password", $pass);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function updateCustomer($id, $name, $lastname, $email, $password, $phone, $adress) {
        $db = DBInit::getInstance();

        $pass = password_hash($password, PASSWORD_DEFAULT);
        
        $statement = $db->prepare("UPDATE users SET name = :name, lastname = :lastname,
            email = :email, password = :password, phone = :phone, adress = :adress WHERE id =:id");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":lastname", $lastname);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":password", $pass);
        $statement->bindParam(":phone", $phone);
        $statement->bindParam(":adress", $adress);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    public static function updateActiveness($id, $name, $lastname, $email, $active, $phone, $adress) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("UPDATE users SET name = :name, lastname = :lastname,
            email = :email, active = :active, phone =:phone, adress =:adress WHERE id =:id");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":lastname", $lastname);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":phone", $phone);
        $statement->bindParam(":adress", $adress);
        $statement->bindParam(":active", $active, PDO::PARAM_INT);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function updateSeller($id, $name, $lastname, $email, $active) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("UPDATE users SET name = :name, lastname = :lastname,
            email = :email, active = :active WHERE id =:id");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":lastname", $lastname);
        $statement->bindParam(":email", $email);
        $statement->bindParam(":active", $active, PDO::PARAM_INT);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function exists($username) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM users 
            WHERE username =:username");
        $statement->bindParam(":username", $username);
        $statement->execute();

        return $statement->fetch();
    }
    public static function addVisit($visitor_id, $visitor_type) {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("INSERT INTO visits (visitor_id, visitor_type)
            VALUES (:visitor_id, :visitor_type)");
        $statement->bindParam(":visitor_id", $visitor_id);
        $statement->bindParam(":visitor_type", $visitor_type);
        $statement->execute();
    }
}

