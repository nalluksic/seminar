<?php

require_once 'database_init.php';

class DBitems {

    public static function getAll() {
        $db = DBInit::getInstance();
        
        $statement = $db->prepare("SELECT * FROM items");
        $statement->execute();

        return $statement->fetchAll();
    }

    public static function get($id) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM items 
            WHERE id =:id");
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();

        return $statement->fetch();
    }
    
    public static function insert($name, $shape, $size, $color, $price, $seller_id, $image) {
        $db = DBInit::getInstance();
        $image = "static/images/" . $image;
        $statement = $db->prepare("INSERT INTO items (name, seller_id, shape, size, color, price, image)
            VALUES (:name, :seller_id, :shape, :size, :color, :price, :image)");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":seller_id", $seller_id);
        $statement->bindParam(":shape", $shape);
        $statement->bindParam(":size", $size);
        $statement->bindParam(":color", $color);
        $statement->bindParam(":price", $price);
        $statement->bindParam(":image", $image);
        $statement->execute();
    }
    
    public static function exists($name) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("SELECT * FROM items 
            WHERE name =:name");
        $statement->bindParam(":name", $name);
        $statement->execute();

        return $statement->fetch();
    }
    
    public static function updateProduct($id, $name, $shape, $size, $color, $price, $active, $image) {
        $db = DBInit::getInstance();

        $statement = $db->prepare("UPDATE items SET name = :name, shape = :shape,
            size = :size, color = :color, price = :price, active = :active, image = :image WHERE id =:id");
        $statement->bindParam(":name", $name);
        $statement->bindParam(":shape", $shape);
        $statement->bindParam(":size", $size);
        $statement->bindParam(":color", $color);
        $statement->bindParam(":price", $price);
        $statement->bindParam(":active", $active, PDO::PARAM_INT);
        $statement->bindParam(":image", $image);
        $statement->bindParam(":id", $id, PDO::PARAM_INT);
        $statement->execute();
    }
    
    public static function getAllAPI(array $prefix) {
         return DBInit::query("SELECT id, name, size,price,image, "
                        . "          CONCAT(:prefix, id) as uri "
                        . "FROM items "
                        . "ORDER BY id ASC", $prefix);
    }
    
    public static function getOneAPI(array $id) {
        $items = DBInit::query("SELECT id, name, shape, size, color, price, image"
                        . " FROM items"
                        . " WHERE id = :id", $id);

        if (count($items) == 1) {
            return $items[0];
        } else {
            throw new InvalidArgumentException("Izdelek ne obstaja");
        }
    } 
}


