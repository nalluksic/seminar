<?php

require_once("model/database_items.php");
require_once("ApiHelper.php");

class ItemsRESTController {

    public static function get($id) {
        try {
            echo ApiHelper::renderJSON(DBitems::getOneAPI(["id" => $id]));
        } catch (InvalidArgumentException $e) {
            echo ApiHelper::renderJSON($e->getMessage(), 404);
        }
    }

    public static function getAll() {
        $prefix = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["HTTP_HOST"]
                . $_SERVER["REQUEST_URI"] . "/";
        echo ApiHelper::renderJSON(DBitems::getAllAPI(["prefix" => $prefix]));
    }

}
