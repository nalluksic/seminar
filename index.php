<?php

session_start();

require_once("ApiHelper.php");
require_once("controllers/ItemsRestController.php");

define("BASE_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php"));
define("IMAGES_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/images/");
define("CSS_URL", rtrim($_SERVER["SCRIPT_NAME"], "index.php") . "static/");

$path = isset($_SERVER["PATH_INFO"]) ? trim($_SERVER["PATH_INFO"], "/") : "";

$urls = [
    
    "/^$/" => function () {
        ApiHelper::redirect(BASE_URL . "login.php");
    },
    //GET ONE
    "/^api\/items\/(\d+)$/" => function ($method, $id = null) {
            ItemsRESTController::get($id);
       
    },
    //GET   
    "/^api\/items$/" => function ($method, $id = null) {
      ItemsRESTController::getAll();
        
    }
];

foreach ($urls as $pattern => $controller) {

    if (preg_match($pattern, $path, $params)) {
        try {
            $params[0] = $_SERVER["REQUEST_METHOD"];
           
            $controller(...$params);
        } catch (InvalidArgumentException $e) {
            ApiHelper::error404();
        } catch (Exception $e) {
            ApiHelper::displayError($e, true);
        }

        exit();
    }
}

ApiHelper::displayError(new InvalidArgumentException("No controller matched."), true);