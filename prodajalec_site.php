<?php
    session_start();
    require_once 'model/database_items.php';
    require_once 'model/database_users.php';
    require_once 'model/database_orders.php';

    if(isset($_SESSION["type"])):
        if($_SESSION["type"]=="admin"):
            header("Location: admin_site.php");
        elseif ($_SESSION["type"]=="stranka"):
            header("Location: stranka_site.php");
        endif;
    else:
        header("Location: index.php");
    endif;
    
    if (!isset($_SERVER["HTTPS"])) {
        $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        header("Location: " . $url);
    }

    $id= $_SESSION["user_id"];
    $name =$_SESSION["name"];
 
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="static/styles.css">
        <script type="text/javascript" src="static/check.js"></script>
        <title>Prodajalec</title>
    </head>
    <body>
       
            <div class="header">
                <img id="logo" src="static/images/logo.png">
            </div>
            <div class="header-links">

               <a  href="<?= basename(__FILE__) . "?do=edit&id=$id" ?>">Uredi profil</a>
               <a  href="<?= basename(__FILE__) . "?do=add_user" ?>">Dodaj stranke</a>
               <a  href="<?= basename(__FILE__) . "?do=edit_users" ?>">Nastavitve stranke</a>
               <a  href="<?= basename(__FILE__) . "?do=add_article" ?>">Dodaj produkt</a>
               <a  href="<?= basename(__FILE__) . "?do=edit_products" ?>">Nastavitve produktov</a>
               <a  href="<?= basename(__FILE__) . "?do=show_orders&finished=1" ?>">Prikaz obdelanih naročil</a>
               <a  href="<?= basename(__FILE__) . "?do=show_orders&finished=0" ?>">Prikaz neobdeanih naročil</a>
               <a style="float:right" href="logout.php">Odjava</a>
            </div>
       
        
         <?php
         
//--------------------------------------------ADD USER-------------------------------------------------------------------------------------------------
       
        if (isset($_GET["do"]) && $_GET["do"] == "add_user"):
            ?>
            <div class="center">
               <h3>Dodaj stranko</h3>
               
                <form action="<?= basename(__FILE__) ?>" method="post">
                    <input type="hidden" name="do" value="add_user" />
                    Ime: <input type="text" name="name" placeholder="Ime"/> <br/>
                    Priimek: <input type="text" name="lastname" placeholder="Priimek"/> <br/>
                    Uporabniško ime: <span id="txt"></span>
                    <input type="text" name="username" placeholder="Uporabniško ime" onkeyup="checkName(this.value)"/> <br/>
                    Email: <input type="email" name="email" placeholder="you@domain"/> <br/>
                    Telefon: <input type="text" name="phone" placeholder="040000000"/> <br/>
                    Naslov: <input type="text" name="adress" placeholder="Ljubljana 1"/> <br/>
                    Geslo: <input type="password" name="password_cmp" placeholder="vsaj 4 znake"/> <br/>
                    Ponovi geslo: <input type="password" name="password" value=""/> <br/>
                   <button type="submit" class="button-blue">Dodaj</button>
                </form>
               
            </div>
            <?php
        
        elseif (isset($_POST["do"]) && $_POST["do"] == "add_user"):
            ?>
            <h3>Dodajanje prodajalca</h3>
            <?php

            if ($_POST["password"]==$_POST["password_cmp"] && strlen($_POST["password_cmp"]) > 3):

                $user = DBUsers::exists($_POST["username"]);
                if($user!=NULL):
                    
                    echo "Uporabniško ime je zasedeno <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
                else:
                    
                    $type = "stranka"; 
                    DBUsers::insertCustomer($_POST["name"], $_POST["lastname"], $_POST["username"], $_POST["email"], $_POST["password"], $type, $_POST["phone"], $_POST["adress"]);
                    $l = basename(__FILE__);
                    echo "Uporabnik dodan. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
               endif;     

           else:
               
               echo "Gesli se ne ujemata ali pa je prekratko. <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
           endif;
        
//--------------------------------------------EDIT CURRENT USER-------------------------------------------------------------------------------------------------
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit"):
            
            try {
            
                $user= DBUsers::get($_GET["id"]);
            } catch (Exception $e) {
                
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            
            $id = $user["id"];
            $name =  $user["name"];
            $lastname =  $user["lastname"];
            $email =  $user["email"];
            $password = $user["password"];
            $type = $user["type"];
     
            ?>
            <div class="center">
               <h3>Uredi svoj profil</h3>
               
               <form action="<?= basename(__FILE__) ?>" method="post">
                   <input type="hidden" name="id" value="<?= $id ?>" />
                   <input type="hidden" name="do" value="edit" />
                   Nov ime: <input type="text" name="name" value="<?= $name ?>"/> <br/>
                   Nov priimek: <input type="text" name="lastname" value="<?= $lastname ?>"/> <br/>
                   Nov email: <input type="email" name="email" value="<?= $email ?>"/> <br/>
                   Novo geslo: <input type="password" name="password_cmp" /> <br/>
                   Ponovi geslo: <input type="password" name="password"/> <br/>
                   <button type="submit" class="button-blue">Shrani</button>
               </form>
               
            </div>
            <?php
        
        elseif (isset($_POST["do"]) && $_POST["do"] == "edit"):
             ?>
            <h3>Posodobitev gesla</h3>
            
            <?php
            if ($_POST["password"]==$_POST["password_cmp"] && strlen($_POST["password_cmp"]) > 3):
                DBUsers::update($_POST["id"], $_POST["name"], $_POST["lastname"], $_POST["email"], $_POST["password"]);
                $l = basename(__FILE__);
                echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
            else:
                echo "Gesli se ne ujemata ali pa je prekratko. <a  class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
            endif;
            
//--------------------------------------------SHOW USERS-------------------------------------------------------------------------------------------------
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_users"):
            
            try {
                $all_users= DBUsers::getAllCustomers(); // POIZVEDBA V PB
            } catch (Exception $e) {
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            ?>
     
                <h3>Uredi stranko</h3>
                <table id="sellers" style="align">
                    <tr>
                       <th>Uporabniško ime</th>
                       <th>Email</th>
                       <th>Aktiven</th>
                     </tr>

                <?php
                foreach ($all_users as $num => $user):
                
                    $url = basename(__FILE__) . "?do=edit_user&id=" . $user["id"];
                    $username = $user["username"];
                    $active = $user["active"];
                
                    if ($active):
                        $active = "&#10004;";
                    else:
                        $active = "&#10008;";
                    endif;
                
                    ?>
              
                    <tr>
                        <td><?= $username ?></td>
                        <td><?= $user["email"] ?></td>
                        <td><?= $active ?></td>
                        <td> <a href="<?= $url ?>"><img src="static/images/edit.png" style="width:25px; height:25px"></a></td>
                    </tr>
                <?php
                endforeach;
            
                ?>
            </table>
     
            <?php
            
//--------------------------------------------EDIT USER-------------------------------------------------------------------------------------------------
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_user"):
                
            $user= DBUsers::get($_GET["id"]);
            $id = $user["id"];
            $name =  $user["name"];
            $lastname =  $user["lastname"];
            $phone =  $user["phone"];
            $adress =  $user["adress"];
            $email =  $user["email"];
            $active =  $user["active"];
                
            if ($active):
                $active = "checked";
            else:
                $active = "unchecked";
            endif;
                
            ?>
            <div class="center">
                <h3>Uredi profil</h3>
                   
                <form action="<?= basename(__FILE__) ?>" method="post">
                    <input type="hidden" name="id" value="<?= $id ?>" />
                    <input type="hidden" name="do" value="edit_user" />
                    Nov ime: <input type="text" name="name" value="<?= $name ?>"/> <br/>
                    Nov priimek: <input type="text" name="lastname" value="<?= $lastname ?>"/> <br/>
                    Nov email: <input type="email" name="email" value="<?= $email ?>"/> <br/>
                    Nov telefon: <input type="text" name="phone" value="<?= $phone ?>"/> <br/>
                    Nov naslov: <input type="text" name="adress" value="<?= $adress ?>"/> <br/>
                    Aktiven: <input type="checkbox" name="active" <?= $active ?>/> <br/>
                    <button type="submit" class="button-blue">Shrani</button>
                </form>
                  
            </div>
            <?php
            
        elseif (isset($_POST["do"]) && $_POST["do"] == "edit_user"):
             ?>
            <h3>Posodobitev profila</h3>
            <?php
            
            if (isset($_POST["active"])):
                    $active = "1";
                else:
                    $active = "0";
                endif;

            DBUsers::updateActiveness($_POST["id"], $_POST["name"], $_POST["lastname"], $_POST["email"], $active, $_POST["phone"], $_POST["adress"]);
            $l = basename(__FILE__);
            $_SESSION["name"]=$_POST["name"];
            echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
        
//--------------------------------------------ADD ARTICLE-------------------------------------------------------------------------------------------------
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "add_article"):
            ?>
            <div class="center">
               <h3>Dodaj produkt</h3>
               
               <form action="<?= basename(__FILE__) ?>" method="post">
        
                   <input type="hidden" name="do" value="add_article" />
                   Ime: <input type="text" name="name"/> <br/>
                   Model: <input type="text" name="shape" /> <br/>
                   Velikost: <input type="text" name="size"/> <br/>
                   Barva: <input type="test" name="color"/> <br/>
                   Cena: <input type="int" name="price"/> <br/>
                   Slika: <input type="file" name="pic" accept="image/*"/> <br/>
                   <button type="submit" class="button-blue">Dodaj</button>
               </form>
            </div>
            <?php
        
        elseif (isset($_POST["do"]) && $_POST["do"] == "add_article"):
            ?>
            <h3>Dodajanje produkta</h3>
            <?php
                
                if($_POST["name"]==NULL || $_POST["shape"]==NULL || $_POST["size"]==NULL || $_POST["color"]==NULL || $_POST["price"]==NULL ):
                   echo "Podajte vse atribute  <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
                else:
                    $product = DBitems::exists($_POST["name"]);
                endif; 
                
                if($product!=NULL):

                    echo "Izdelek je že dodan. <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";

                else:
                    if (!isset($_POST["pic"])):
                        $_POST["pic"]="no-image.jpg";
                    endif;              
                   
                    DBitems::insert($_POST["name"],  $_POST["shape"], $_POST["size"], $_POST["color"], $_POST["price"], $_SESSION["user_id"], $_POST["pic"]);
                    $l = basename(__FILE__);
                    echo "Produkt dodan. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
               endif;
               
//--------------------------------------------EDIT PRODUCTS-------------------------------------------------------------------------------------------------
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_products"):
            
            try {
                $all_products= DBitems::getAll(); // POIZVEDBA V PB
            } catch (Exception $e) {
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            ?>
            <div class="center">
               <h3>Uredi produkte</h3>
               <table id="sellers" style="align">
                <tr>
                   <th>Ime</th>
                   <th>Aktiven</th>
                 </tr>
            
                <?php
                foreach ($all_products as $num => $item):
               
                    $url = basename(__FILE__) . "?do=edit_product&id=" . $item["id"];
                    $name = $item["name"];
                    $active = $item["active"];
                    if ($active):
                        $active = "&#10004;";
                    else:
                        $active = "&#10008;";
                   
                    endif;
                     ?>
                     <tr>
                        <td><?= $name ?></td>
                        <td><?= $active ?></td>
                        <td> <a href="<?= $url ?>"><img src="static/images/edit.png" style="width:25px; height:25px"></a></td>
                    </tr>
              
               <?php
                
                endforeach;
                ?>
               </table>
            </div>
     
            <?php
            
           //--------------------------------------------EDIT PRODUCT-------------------------------------------------------------------------------------------------
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_product"):
                 
            $item= DBitems::get($_GET["id"]);
            $active =  $item["active"];
          
            if ($active):
                $active = "checked";
            else:
                $active = "unchecked";
            endif;
                ?>
                <div class="center">
                   <h3>Uredi produkt</h3>
                   
                   <form action="<?= basename(__FILE__) ?>" method="post">
                        <input type="hidden" name="id" value="<?= $item["id"] ?>" />
                        <input type="hidden" name="do" value="edit_product" />
                        <input type="hidden" name="img" value="<?= $item["image"] ?>" />
                        Novo ime: <input type="text" name="name" value="<?= $item["name"] ?>"/> <br/>
                        Nova oblika: <input type="text" name="shape" value="<?= $item["shape"] ?>"/> <br/>
                        Nova barva: <input type="text" name="color" value="<?= $item["color"] ?>"/> <br/>
                        Nova velikost: <input type="text" name="size" value="<?= $item["size"] ?>"/> <br/>
                        Nova cena: <input type="int" name="price" value="<?= $item["price"] ?>"/> <br/>
                        
                        Aktiven: <input type="checkbox" name="active" <?= $active ?>/> <br/>
                        Zamenjaj sliko: <input type="file" name="pic" accept="image/*"/> <br/>
                        Slika:<img style="width:300px; height:auto;" src="<?= $item["image"] ?>">
                        <button type="submit" class="button-blue">Shrani</button>
                   </form>
                   
                </div>
                <?php
        elseif (isset($_POST["do"]) && $_POST["do"] == "edit_product"):
            ?>
            <h3>Posodobitev izdelka</h3>
            <?php
       
            if (isset($_POST["active"])):
                $active = "1";
            else:
                $active = "0";
            endif;
            
            $pic="" . $_POST["pic"];
            if (strlen($pic)==0):
                $pic=$_POST["img"];
            
            else:
                $pic="static/images/" . $_POST["pic"];
            endif;
            
         
                 
                  
            DBitems::updateProduct( $_POST["id"], $_POST["name"], $_POST["shape"], $_POST["size"], $_POST["color"], $_POST["price"], $active, $pic);
            $l = basename(__FILE__);
            echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
        elseif (isset($_GET["do"]) && $_GET["do"] == "show_orders"):
               
            try {
                $all_orders= DBorders::getAll(); // POIZVEDBA V PB
            } catch (Exception $e) {
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            
            ?>
            <div class="center1">
               <h3>Uredi naročila</h3>
              
                <table frame="box" class="order-table" rules="groups" cellpadding="7">

                <thead>
                    <tr>
                        <th> ID naročila </th>
                        <th> ID stranke </th>
                        <th> Datum </th> 
                        <th> Cena </th>
                        <th> Opravljeno </th>
                        <th> Preklicano </th>
                    </tr>
                </thead>
                <?php
                foreach ($all_orders as $num => $order) {
                
                    $url = basename(__FILE__) . "?do=edit_order&id=" . $order["id"];
                    $order_id = $order["id"];
                    $costumer_id = $order["costumer_id"];
                    $amount = $order["amount"];
                    $date = $order["date"];
                    $done = $order["done"];
                
                    if ($done):
                        $done = "&#10004;";
                    else:
                        $done = "&#10008;";
                    endif;
                
                    $canceled = $order["canceled"];
                
                    if ($canceled):
                        $canceled = "&#10004;";
                    else:
                        $canceled = "&#10008;";
                    endif;
                    if($_GET["finished"]==$order["done"]):
                
                        ?>
                        <tbody>
                            <tr>
                                <td  align="center"> <?php echo $order_id ?> </td>
                                <td  align="center"> <?php echo $costumer_id ?> </td>
                                <td  align="center"> <?php echo $date ?> </td> 
                                <td  align="center"> <?php echo $amount ?> 
                                <td  align="center"> <b><?php echo $done ?></b> </td>
                                <td  align="center"> <b><?php echo $canceled ?></b> </td>
                               <td> <a href="<?= $url ?>"><img src="static/images/edit.png" style="width:25px; height:25px"></a></td>
                            </tr>
                        </tbody>
                <?php endif;  }  ?>
                </table>
            </div>
            <?php
             
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_order"):
                
            $all_items= DBorders::getAllItems($_GET["id"]);
            $order= DBorders::get($_GET["id"]);
            $customer = DBUsers::get($order["costumer_id"]);
            $done = $order["done"];
                 
            if($done):
                $done = "checked";
            else:
                $done = "unchecked";
            endif;
                
            $canceled = $order["canceled"];
                 
            if($canceled):
                $canceled = "checked";
            else:
                $canceled = "unchecked";
            endif;
            ?>
            
            <div class="center1">
               <h3>Produkti naročila</h3>
                <table frame="box" cellpadding="7">
                    <thead>
                        <tr>
                            <th> ID naročnika </th>
                            <th> Ime naročnika </th>
                            <th> Priimek naročnika  </th> 
                            <th> Naslov naročnika  </th>
                            <th> Telefon naročnika  </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td  align="center"> <?php echo $customer["id"] ?> </td> 
                            <td  align="center"> <?php echo $customer["name"] ?> </td> 
                            <td  align="center"> <?php echo $customer["lastname"] ?> </td>
                            <td  align="center"> <?php echo $customer["adress"] ?> </td>
                            <td  align="center"> <?php echo $customer["phone"] ?> </td>
                        </tr>
                    </tbody>
                </table>
            
                <br>
                <table frame="box" class="order-table" rules="groups" cellpadding="7">
                    <thead>
                        <tr>
                            <th> ID produkta </th>
                            <th> ime produkta </th>
                            <th> Količina </th>
                            <th> Cena </th>
                        </tr>
                    </thead>
                <?php
                foreach ($all_items as $num => $info) {
                
                    $item= DBitems::get($info["item_id"]);
                    ?>
                    <tbody>
                        <tr>
                            <td  align="center"> <?php echo $info["item_id"] ?> </td> 
                            <td  align="center"> <?php echo $item["name"] ?> </td>
                            <td  align="center"> <?php echo $info["volume"] ?> </td> 
                            <td  align="center"> <?php echo $item["price"] ?> </td> 
                        </tr>
                    </tbody>
                <?php  }  ?>
                </table>
                <br/>
                
                <form action="<?= basename(__FILE__) ?>" method="post">
                    <input type="hidden" name="do" value="edit_order" />
                    <input type="hidden" name="id" value="<?= $_GET["id"] ?>" />
                    Potrdi naročilo:<input type="checkbox" name="done" <?= $done ?>/> <br/>
                    <p>Prekliči naročilo:</p> <input type="checkbox" name="canceled" <?= $canceled ?>/> <br/>
                    <button type="submit" class="button-blue">Shrani</button>
                </form>
                
            </div>
            <?php     
        elseif (isset($_POST["do"]) && $_POST["do"] == "edit_order"):
            ?>
            <h3>Posodobitev naročila</h3>
            
            <?php
            
            if (isset($_POST["done"])):
                $done = "1";
            else:
                $done = "0";
            endif;
               
            if (isset($_POST["canceled"])):
                $canceled = "1";
            else:
                $canceled = "0";
            endif;
                
            DBorders::update($_POST["id"], $done, $canceled);
            $l = basename(__FILE__);
            echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
            
        else:
           
           echo "<div class='center'><p>Pozdravljen, $name</p></div>";
        endif;
        ?>
    </body>
</html>