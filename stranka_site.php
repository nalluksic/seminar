<?php
    session_start();
    require_once 'model/database_users.php';
    require_once 'model/database_orders.php';
    require_once 'model/database_items.php';
    
    if(isset($_SESSION["type"])):
        if ($_SESSION["type"]=="prodajalec"):
            header("Location: prodajalec_site.php");
        elseif ($_SESSION["type"]=="admin"):
            header("Location: admin_site.php");
        endif;
    else:
        header("Location: index.php");
        exit;
    endif;
    
    if (!isset($_SERVER["HTTPS"])) {
        $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        header("Location: " . $url);
    }
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="static/styles.css">
        <title>Admin</title>
        
    </head>
    <body>
        <?php
            $id= $_SESSION["user_id"];
            $name =$_SESSION["name"];
          
            
            ?>
            
            <div class="header">
                <img id="logo" src="static/images/logo.png">
            </div>
            <div class="header-links">
                <a  href="<?= basename(__FILE__) . "?do=edit&id=$id" ?>">Uredi profil</a>
                <a  href="shop.php">Trgovina</a>
                <a  href="<?= basename(__FILE__) . "?do=show_orders" ?>">Ogled preteklih nakupov</a>
                <a style="float:right" href="logout.php">Odjava</a>
            </div>
        
     
        
        <?php
        if (isset($_GET["do"]) && $_GET["do"] == "edit"):
            
            try {
                $user= DBUsers::get($_GET["id"]); // POIZVEDBA V PB
            } catch (Exception $e) {
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            
            $id = $user["id"];
            $name =  $user["name"];
            $lastname =  $user["lastname"];
            $email =  $user["email"];
            $password = $user["password"];
            $phone = $user["phone"];
            $adress = $user["adress"];
            $type = $user["type"];
            
            ?>
            <div class="center">
               <h2>Uredi profil</h2>
               
               <form action="<?= basename(__FILE__) ?>" method="post">
                   <input type="hidden" name="id" value="<?= $id ?>" />
                   <input type="hidden" name="do" value="edit" />
                   Nov ime: <input type="text" name="name" value="<?= $name ?>"/> <br/>
                   Nov priimek: <input type="text" name="lastname" value="<?= $lastname ?>"/> <br/>
                   Nov email: <input type="email" name="email" value="<?= $email ?>"/> <br/>
                   Nov telefon: <input type="text" name="phone" value="<?= $phone ?>"/> <br/>
                   Nov naslov: <input type="text" name="adress" value="<?= $adress ?>"/> <br/>
                   Novo geslo: <input type="password" name="password_cmp" /> <br/>
                   Ponovi geslo: <input type="password" name="password"/> <br/>
                   <button type="submit" class="button-blue">Shrani</button>
               </form>
            </div>
            <?php
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_order"):
            
            
            $all_items= DBorders::getAllItems($_GET["id"]);
            $order= DBorders::get($_GET["id"]);
            $done = $order["done"];
            
            if($done):
                $done = "checked";
            else:
                $done = "unchecked";
            endif;
                
            $canceled = $order["canceled"];
                 
            if($canceled):
                $canceled = "checked";
            else:
                $canceled = "unchecked";
            endif;
            ?>
            
            <div class="center1">
               <h3>Produkti naročila</h3>
            
                <br>
                <table frame="box" class="order-table" rules="groups" cellpadding="7">
                    <thead>
                        <tr>
                            <th> ID produkta </th>
                            <th> ime produkta </th>
                            <th> Količina </th>
                            <th> Cena </th>
                        </tr>
                    </thead>
                <?php
                foreach ($all_items as $num => $info) {
                
                    $item= DBitems::get($info["item_id"]);
                    ?>
                    <tbody>
                        <tr>
                            <td  align="center"> <?php echo $info["item_id"] ?> </td> 
                            <td  align="center"> <?php echo $item["name"] ?> </td>
                            <td  align="center"> <?php echo $info["volume"] ?> </td> 
                            <td  align="center"> <?php echo $item["price"] ?> </td> 
                        </tr>
                    </tbody>
                <?php  }  ?>
                </table>
                <br/>

            </div>
            <?php     

        elseif (isset($_POST["do"]) && $_POST["do"] == "edit"):
            ?>
            <h1>Posodobitev gesla</h1>
            
            <?php
            if ($_POST["password"]==$_POST["password_cmp"] && strlen($_POST["password_cmp"]) > 3):
                DBUsers::updateCustomer($_POST["id"], $_POST["name"], $_POST["lastname"], $_POST["email"], $_POST["password"], $_POST["phone"], $_POST["adress"]);
                $l = basename(__FILE__);
                $_SESSION["name"]=$_POST["name"];
                echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
            else:
                echo "Gesli se ne ujemata ali pa je prekratko. <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
            endif;
          
        elseif (isset($_GET["do"]) && $_GET["do"] == "shop"):
            header("Location: mail.html");
        //default stran
        elseif (isset($_GET["do"]) && $_GET["do"] == "show_orders"):
            try {
                $all_orders= DBorders::getAll(); // POIZVEDBA V PB
            } catch (Exception $e) {
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            ?>
            <div class="center1">
               <h1>Uredi naročila</h1>
              
                <table frame="box" class="order-table" rules="groups" cellpadding="7">
                    <thead>
                        <tr>
                            <th> Datum </th> 
                            <th> Cena </th>
                            <th> Opravljeno </th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($all_orders as $num => $order) {
                        if($order["costumer_id"]==$_SESSION["user_id"]):
                        
                            $price = $order["amount"];
                            $date = $order["date"];
                            $done = $order["done"];
                            $url = basename(__FILE__) . "?do=edit_order&id=" . $order["id"];
            
                            if ($done):
                                $done = "&#10004;";
                            else:
                                $done = "&#10008;";
                            endif;
                    ?>
                    <tbody>
                        <tr>
                            <td  align="center"> <?php echo $date ?> </td> 
                            <td  align="center"> <?php echo $price ?> 
                            <td  align="center"> <b><?php echo $done ?></b> </td>
                            <td> <a href="<?= $url ?>"><img src="static/images/edit.png" style="width:25px; height:25px"></a></td>
                        </tr>
                    </tbody>
                    
                    <?php endif;  }  ?>
                </table>
            </div>
            <?php
        else:
               echo "<div class='center'><p>Pozdravljen, $name</p></div>";
           
         endif; ?>
    </body>
</html>