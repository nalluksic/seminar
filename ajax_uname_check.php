<?php

$q = $_REQUEST["q"];

$dbh = new PDO("mysql:host=localhost;dbname=shop", "root", "ep");
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

$query = "SELECT id FROM users WHERE username = ?";
$stmt = $dbh->prepare($query);
$stmt->bindValue(1, $q);

$stmt->execute();
$user = $stmt->fetchObject();

if ($user):
    echo '<font size="3" color="red">&#10008;</font>';
else:
    echo '<font size="3" color="green">&#10004;</font>';
endif;
