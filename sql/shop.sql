DROP DATABASE IF EXISTS shop;
CREATE DATABASE shop;
USE shop;

-- tabela jokes
DROP TABLE IF EXISTS users;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `lastname` text NOT NULL,
  `username` varchar(20) NOT NULL UNIQUE,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) COLLATE utf8_slovenian_ci NOT NULL,
  `type` text NOT NULL,
  `phone` text default NULL,
  `adress` text default NULL,
  `active` int(1) default 1,
  PRIMARY KEY (`id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- vnosi v tabelo
INSERT INTO `users`(name, lastname, username, email, password, type, phone, adress) VALUES ("Nal", "Luksic", "nluksic", "nal.luksic@gmail.com", "$2y$10$w43ZYR5g/etykT9XWxbgZeyQ8NRFJGJ1sJR5VgXE3njGYUP1Av6mC", "admin", null, null);
INSERT INTO `users`(name, lastname, username, email, password, type, phone, adress) VALUES ("Matic", "Oblak", "moblak", "matic.oblak@gmail.com", "$2y$10$w43ZYR5g/etykT9XWxbgZeyQ8NRFJGJ1sJR5VgXE3njGYUP1Av6mC", "prodajalec", null, null);
INSERT INTO `users`(name, lastname, username, email, password, type, phone, adress) VALUES ("Miha", "Piha", "mihapiha", "miha.piha@gmail.com", "$2y$10$w43ZYR5g/etykT9XWxbgZeyQ8NRFJGJ1sJR5VgXE3njGYUP1Av6mC", "prodajalec", null, null);
INSERT INTO `users`(name, lastname, username, email, password, type, phone, adress) VALUES ("Ana", "Prana", "anaprana", "ana.prana@gmail.com", "$2y$10$w43ZYR5g/etykT9XWxbgZeyQ8NRFJGJ1sJR5VgXE3njGYUP1Av6mC", "stranka", "040321321", "Ljubljana 12");
INSERT INTO `users`(name, lastname, username, email, password, type, phone, adress) VALUES ("Janez", "Novak", "janez", "janeznovak@gmail.com", "$2y$10$w43ZYR5g/etykT9XWxbgZeyQ8NRFJGJ1sJR5VgXE3njGYUP1Av6mC", "stranka", "040111111", "Ljubljana 1");

-- tabela orders
DROP TABLE IF EXISTS orders;
CREATE TABLE `orders` (
    `id` int(11) NOT NULL AUTO_INCREMENT, 
    `costumer_id` int(11) NOT NULL, 
    `date` date NOT NULL,
    `amount` float(1) default 1,
    `done` int(1) default 0, 
    `canceled` int(1) default 0, 
    PRIMARY KEY (`id`),
    FOREIGN KEY(`costumer_id`) references users(`id`) on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `orders`(costumer_id, amount, date, done, canceled) VALUES (4, 199.98, NOW(), "0", "0");
INSERT INTO `orders`(costumer_id, amount, date, done, canceled) VALUES (4, 199.98, NOW(), "0", "0");
INSERT INTO `orders`(costumer_id, amount, date, done, canceled) VALUES (4, 199.98, NOW(), "0", "0");
-- tabela items

DROP TABLE IF EXISTS items;
CREATE TABLE `items` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `seller_id` int(11) NOT NULL,  
    `name` text NOT NULL,
    `shape` text NOT NULL,
    `size` varchar(20) NOT NULL,
    `color` varchar(30) NOT NULL,
    `price` float(1) default 1, 
    `active` int(1) default 1,
    `image` text NOT NULL,  
    PRIMARY KEY (`id`),
    FOREIGN KEY(`seller_id`) references users(`id`) on delete cascade
  
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `items`(seller_id, name, shape, size, color, price, image) VALUES ("2", "Polo majica", "polo", "L", "Rdeča", "19.99", "static/images/polo.jpg");
INSERT INTO `items`(seller_id, name, shape, size, color, price, image) VALUES ("3", "Hlače", "kratke", "32", "Modra", "39.99", "static/images/shorts.jpg");
INSERT INTO `items`(seller_id, name, shape, size, color, price, image) VALUES ("3", "Trenirka Adidas", "s črtami", "M", "Modra", "15.49", "static/images/adidas.jpg");
INSERT INTO `items`(seller_id, name, shape, size, color, price, image) VALUES ("2", "Pulover", "z vzorcem", "XL", "Modra", "17.80", "static/images/sweater.jpg");
INSERT INTO `items`(seller_id, name, shape, size, color, price, image) VALUES ("3", "Spodnja majica", "bombaž", "M", "Bela", "5.89", "static/images/undershirt.jpeg");
INSERT INTO `items`(seller_id, name, shape, size, color, price, image) VALUES ("2", "Kratka majica", "V izrez", "M", "Siva", "6.99", "static/images/tshirt.jpg");

DROP TABLE IF EXISTS item_orders;
CREATE TABLE `item_orders` (
    `order_id` int(11) NOT NULL,  
    `item_id` int(11) NOT NULL,
    `volume` int(2) default 1,
    PRIMARY KEY (`order_id`, `item_id`),
    FOREIGN KEY(`order_id`) references orders(`id`) on delete cascade,
    FOREIGN KEY(`item_id`) references items(`id`) on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `item_orders`(order_id, item_id, volume) VALUES (1, 1, 20);
INSERT INTO `item_orders`(order_id, item_id, volume) VALUES (1, 2, 10);
INSERT INTO `item_orders`(order_id, item_id, volume) VALUES (1, 3, 2);

DROP TABLE IF EXISTS visits;
CREATE TABLE `visits` (
    `visitor_id` int(11) NOT NULL,  
    `visitor_type` text NOT NULL,
    `date_time` TIMESTAMP default NOW(),
    FOREIGN KEY(`visitor_id`) references users(`id`) on delete cascade
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `visits`(visitor_id, visitor_type, date_time) VALUES (1, "admin", NOW()) ;

