<?php

session_start();
require_once 'model/database_users.php';

  if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}

?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="static/styles.css">
        <script type="text/javascript" src="static/check.js"></script>
        <title>Pozdravljen nov uporabnik</title>
    </head>
    <body> 
        <div class="header">
            <img id="logo" src="static/images/logo.png">
        </div>
        <div class="header-links">
            <?php
                echo '<a href="login.php">Nazaj na login</a>';
              
            ?>
         
            
        </div>
        <?php
        if (isset($_POST["do"]) && $_POST["do"] == "add_user"):
        ?>
            <h1>Registracija novega uporabnika</h1>
            <?php
            if ($_POST["password"]==$_POST["password_cmp"] && strlen($_POST["password_cmp"]) > 3):

                $user = DBUsers::exists($_POST["username"]);
                if($user!=NULL):
                    echo "Uporabniško ime je zasedeno <a href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
                else:
                    $type = "stranka"; 
                    DBUsers::insertCustomer($_POST["name"], $_POST["lastname"], $_POST["username"], $_POST["email"], $_POST["password"], $type, $_POST["phone"], $_POST["adress"] );
                    $l = basename("/index.php");
                    
                    echo "Uporabnik dodan. <a href='$l'>Na prvo stran.</a></p>";
                endif;     
            else:
                echo "Gesli se ne ujemata ali pa je prekratko. <a href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
            endif;
        else:
            ?>
             <div class="center">
                <h2>Registracija novega uporabnika</h2>
                <form action="<?= basename(__FILE__) ?>" method="post">
                    <input type="hidden" name="do" value="add_user" />
                    Ime: <input type="text" name="name" placeholder="Ime"/> <br/>
                    Priimek: <input type="text" name="lastname" placeholder="Priimek"/> <br/>
                     Uporabniško ime: <span id="txt"></span><input type="text" name="username" placeholder="Uporabniško ime" onkeyup="checkName(this.value)" <br/>
                    Email: <input type="email" name="email" placeholder="you@domain"/> <br/>
                    Telefon: <input type="text" name="phone" placeholder="040000000"/> <br/>
                    Naslov: <input type="text" name="adress" placeholder="Ljubljana 1"/> <br/>
                    Geslo: <input type="password" name="password_cmp" placeholder="vsaj 4 znake"/> <br/>
                    Ponovi geslo: <input type="password" name="password" value=""/> <br/>
                    <input type="submit" value="Dodaj" />
                </form>
            </div>
            <?php
        endif
            ?>  
    </body>
</html>