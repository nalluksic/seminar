<?php

session_start();
require_once 'model/database_users.php';

if(isset($_SESSION["type"])):
    if($_SESSION["type"]=="admin"):
        header("Location: admin_site.php");
    elseif ($_SESSION["type"]=="prodajalec"):
        header("Location: prodajalec_site.php");
    elseif ($_SESSION["type"]=="stranka"):
        header("Location: stranka_site.php");
    endif;
endif;

if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}

 
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/css?family=Barlow" rel="stylesheet"> 
        <link rel="stylesheet" href="static/styles.css">
        
        <title>Prijava</title>
    </head>
    <body>
        <?php
        
       
        
        if (isset($_POST["uname"]) && isset($_POST["password"])):
            try {
                $dbh = new PDO("mysql:host=localhost;dbname=shop", "root", "ep");
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $dbh->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
                
                $query = "SELECT * FROM users WHERE username = ?";
                $stmt = $dbh->prepare($query);
                $stmt->bindValue(1, $_POST["uname"]);
                
                $stmt->execute();
                $user = $stmt->fetch();
                
                $secret = "6LcBKD4UAAAAABBq4CuBMlDGcEurUjZd5LvlwFA4";
                $response = $_POST["g-recaptcha-response"];
                $remoteip = $_SERVER["REMOTE_ADDR"];
                
                $urlGoogle = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$remoteip");
                $resulte = json_decode($urlGoogle, TRUE);
                if($resulte["success"]==1 && $user){
                    
                    if(password_verify($_POST["password"], $user["password"])){
                    
                        $type = $user["type"];
                         
                        $_SESSION["user_id"] = $user["id"];;
                        $_SESSION["type"] = $type;
                        $_SESSION["name"] = $user["name"];;
                        
                        if(strcmp($type, "admin")==0 || strcmp($type, "prodajalec")==0):
                            try {
                                DBUsers::addVisit($user["id"], $type);
                            } catch (Exception $e) {
                                die($e->getMessage());
                            }
                        endif;
                        
                        if (strcmp($type, "admin")==0){
                            
                        header("Location: admin_site.php");
                        }
                        elseif (strcmp($type, "prodajalec")==0){
                            
                            if ($user["active"]==0):
                                echo "Račun deaktiviran. <a class='normal-link' href='$_SERVER[PHP_SELF]'>Nazaj.</a>";
                                $_SESSION=[];
                                session_destroy();
                            else:
                            header("Location: prodajalec_site.php");
                        
                            endif;
                        }
                        elseif (strcmp($type, "stranka")==0){
                            
                            header("Location: stranka_site.php");
                        }
                        else{
                            echo "Napaka!";
                        }
                    } else {
                        echo "Prijava neuspešna. <a class='normal-link' href='$_SERVER[PHP_SELF]'>Nazaj.</a>";
                    }
                }else{
                     echo "Prijava neuspešna. <a class='normal-link' href='$_SERVER[PHP_SELF]'>Nazaj.</a>";
                }   
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
        else:
            ?>
            <div class="center" style="margin-top:100px">
                <img id="logo" src="static/images/logo.png">
                <form  action="<?= basename(__FILE__) ?>" method="post">
                    <div>
                        <input type="text" name="uname" placeholder="Uporabniško ime"/>
                        <input type="password" name="password" placeholder="Geslo" />
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LcBKD4UAAAAAHlp922NsNtEmTd-enESnwHGmwN7"></div>
                   
                    <button class="button-green" type="submit">PRIJAVA</button>
                    
                    <a class="normal-link" onClick="document.location.href='registration.php'">Registriraj se</a>
                    <a class="normal-link" style="float:right" onClick="document.location.href='shop.php'">Nadaljuj kot gost</a>
                    
                </form>
            </div>
        <?php endif; ?>
    </body>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</html>
