<?php
    session_start();
    require_once 'model/database_users.php';

    if(isset($_SESSION["type"])):
        if ($_SESSION["type"]=="prodajalec"):
            header("Location: prodajalec_site.php");
        elseif ($_SESSION["type"]=="stranka"):
            header("Location: stranka_site.php");
        endif;
    else:
        header("Location: index.php");
        exit;
    endif;
    
    if (!isset($_SERVER["HTTPS"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}
?>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="static/styles.css">
        <script type="text/javascript" src="static/check.js"></script>
        <title>Admin</title>
    </head>
    <body>
            <?php
            $id= $_SESSION["user_id"];
            $name =$_SESSION["name"];
           // echo "<p>Pozdravljen, $name";
            
            ?>
            <div class="header">
                <img id="logo" src="static/images/logo.png">
            </div>
            <div class="header-links">
                <a href="<?= basename(__FILE__) . "?do=edit&id=$id" ?>">Uredi profil</a>
                <a href="<?= basename(__FILE__) . "?do=add_user" ?>">Dodaj prodajalca</a>
                <a href="<?= basename(__FILE__) . "?do=edit_users" ?>">Nastavitve prodajalcev</a>
                <a style="float:right" href="logout.php">Odjava</a>
            </div>
                
         
        
     
        
        <?php
         
        if (isset($_GET["do"]) && $_GET["do"] == "add_user"):       
            ?>
            <div class="center">
               <h3>DODAJ PRODAJALCA</h3>
               <form action="<?= basename(__FILE__) ?>" method="post">
       
                   <input type="hidden" name="do" value="add_user" />
                   Ime: <input type="text" name="name" placeholder="Ime"/> <br/>
                   Priimek: <input type="text" name="lastname" placeholder="Priimek"/> <br/>
                   Uporabniško ime: <span id="txt"></span>
                   <input type="text" name="username" placeholder="Uporabniško ime" onkeyup="checkName(this.value)" <br/>
                   Email: <input type="email" name="email" placeholder="you@domain"/> <br/>
                   Geslo: 
                   <input type="password" name="password_cmp" placeholder="Vnesi geslo" /> <br/>
                   Ponovi geslo: <input type="password" name="password"/> <br/>
                   
                   <button type="submit" class="button-blue">Dodaj</button>
                   
               </form>
            </div>
            <?php
        
        //dodajanje prodajalca -POST
        elseif (isset($_POST["do"]) && $_POST["do"] == "add_user"):
            ?>
           <h3>DODAJANJE PRODAJALCA</h3>
           <?php

           if ($_POST["password"]==$_POST["password_cmp"] && strlen($_POST["password_cmp"]) > 3):

               $user = DBUsers::exists($_POST["username"]);
               if($user!=NULL):
                   
                    echo "Uporabniško ime je zasedeno <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
               else:
                   
                $type = "prodajalec"; 
                DBUsers::insert($_POST["name"], $_POST["lastname"], $_POST["username"], $_POST["email"], $_POST["password"], $type);
                $l = basename(__FILE__);
                echo "Uporabnik dodan. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";

               endif;     

           else:
               
               echo "Gesli se ne ujemata ali pa je prekratko. <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
           endif;
        
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit"):
            
            try {
            
                $user= DBUsers::get($_GET["id"]);
            } catch (Exception $e) {
                
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            
            $id = $user["id"];
            $name =  $user["name"];
            $lastname =  $user["lastname"];
            $email =  $user["email"];
            $password = $user["password"];
            $type = $user["type"];

            ?>
            <div class="center">
               <h3>Uredi profil</h3>
               
               <form action="<?= basename(__FILE__) ?>" method="post">
                   <input type="hidden" name="id" value="<?= $id ?>" />
                   <input type="hidden" name="do" value="edit" />
                   Nov ime: <input type="text" name="name" value="<?= $name ?>"/> <br/>
                   Nov priimek: <input type="text" name="lastname" value="<?= $lastname ?>"/> <br/>
                   Nov email: <input type="email" name="email" value="<?= $email ?>"/> <br/>
                   Novo geslo: <input type="password" name="password_cmp" /> <br/>
                   Ponovi geslo: <input type="password" name="password"/> <br/>
                   <button type="submit" class="button-blue">Shrani</button>
               
               </form>
            </div>
            <?php
        
        //admin nastavitve -POST
        elseif (isset($_POST["do"]) && $_POST["do"] == "edit"):
             ?>
            <h3>Posodobitev gesla</h3>
            
            <?php
            if ($_POST["password"]==$_POST["password_cmp"] && strlen($_POST["password_cmp"]) > 3):
                
                DBUsers::update($_POST["id"], $_POST["name"], $_POST["lastname"], $_POST["email"], $_POST["password"]);
                $l = basename(__FILE__);
                $_SESSION["name"]=$_POST["name"];
                echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
            else:
                
                echo "Gesli se ne ujemata ali pa je prekratko. <a class='normal-link' href='$_SERVER[HTTP_REFERER]'>Nazaj.</a></p>";
            endif;
            
        //upravljanje s prodajalci  
        elseif (isset($_GET["do"]) && $_GET["do"] == "edit_users"):
            
            try {
                $all_users= DBUsers::getAllSellers(); // POIZVEDBA V PB
            } catch (Exception $e) {
                echo "Napaka pri poizvedbi: " . $e->getMessage();
            }
            ?>
              
               <h3>Uredi prodajalce</h3>
               <table id="sellers" style="align">
                         <tr>
                            <th>Uporabniško ime</th>
                            <th>Email</th>
                            <th>Aktiven</th>
                          </tr>
            <?php
            foreach ($all_users as $num => $user):
                
            
                $url = basename(__FILE__) . "?do=edit_user&id=" . $user["id"];
                $username = $user["username"];
                $active = $user["active"];
                if ($active):
                    $active = "&#10004;";
                else:
                    $active = "&#10008;";
                   
                endif;
                ?>
              
                <tr>
                    <td><?= $username ?></td>
                    <td><?= $user["email"] ?></td>
                    <td><?= $active ?></td>
                    <td> <a href="<?= $url ?>"><img src="static/images/edit.png" style="width:25px; height:25px"></a></td>
                </tr>
                <?php
            endforeach;        
            
            ?>
                  
            </table>
           
     
            <?php
            
            //upravljanje s prodajalcem - GET  
            elseif (isset($_GET["do"]) && $_GET["do"] == "edit_user"):
                 ?>
                <h3>Uredi prodajalca</h3>
                
                <?php
                
                $user= DBUsers::get($_GET["id"]);
                $id = $user["id"];
                $name =  $user["name"];
                $lastname =  $user["lastname"];
                $email =  $user["email"];
                $active =  $user["active"];
                
                if ($active):
                    $active = "checked";
                else:
                    $active = "unchecked";
                endif;
                
                ?>
                <div class="center">
                   <h2>Uredi profil</h2>
                   
                   <form action="<?= basename(__FILE__) ?>" method="post">
                       <input type="hidden" name="id" value="<?= $id ?>" />
                       <input type="hidden" name="do" value="edit_user" />
                       Nov ime: <input type="text" name="name" value="<?= $name ?>"/> <br/>
                       Nov priimek: <input type="text" name="lastname" value="<?= $lastname ?>"/> <br/>
                       Nov email: <input type="email" name="email" value="<?= $email ?>"/> <br/>
                       Aktiven: <input type="checkbox" name="active" <?= $active ?>/> <br/>
                       <button type="submit" class="button-blue">Shrani</button>
                   </form>
                   
                </div>
                <?php
            
        //upravljanje s prodajalcem - POST
        elseif (isset($_POST["do"]) && $_POST["do"] == "edit_user"):
             ?>
            <h3>Posodobitev gesla</h3>
           
            <?php
            
           
            if (isset($_POST["active"])):
                    $active = "1";
                else:
                    $active = "0";
                endif;

            DBUsers::updateSeller($_POST["id"], $_POST["name"], $_POST["lastname"], $_POST["email"], $active);
            $l = basename(__FILE__);
            echo "Podaki spremenjeni. <a class='normal-link' href='$l'>Na prvo stran.</a></p>";
        
        //default stran
        else:
             echo "<div class='center'><p>Pozdravljen, $name</p></div>";
        endif;
        ?>
                
    
    </body>
</html>