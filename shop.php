<?php
session_start();

require_once 'model/database_items.php';
require_once 'model/database_users.php';
require_once 'model/database_orders.php';

if (!isset($_SERVER["HTTPS"]) && isset($_SESSION["type"])) {
    $url = "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
    header("Location: " . $url);
}

$url = filter_input(INPUT_SERVER, "PHP_SELF", FILTER_SANITIZE_SPECIAL_CHARS);
$validationRules = ['do' => [
        'filter' => FILTER_VALIDATE_REGEXP,
        'options' => [
            "regexp" => "/^(add_into_cart|update_cart|purge_cart|order_cart|confirm)$/"
        ]
    ],
    'id' => [
        'filter' => FILTER_VALIDATE_INT,
        'options' => ['min_range' => 0]
    ],
    'kolicina' => [
        'filter' => FILTER_VALIDATE_INT,
        'options' => ['min_range' => 0]
    ]
];
$data = filter_input_array(INPUT_POST, $validationRules);

switch ($data["do"]) {
    case "add_into_cart":
        try {
            $item = DBitems::get($data["id"]);

            if (isset($_SESSION["cart"][$item["id"]])) {
                $_SESSION["cart"][$item["id"]] ++;
            } else {
                $_SESSION["cart"][$item["id"]] = 1;
            }
        } catch (Exception $exc) {
            die($exc->getMessage());
        }
        break;
    case "update_cart":
        if (isset($_SESSION["cart"][$data["id"]])) {
            if ($data["kolicina"] > 0) {
                $_SESSION["cart"][$data["id"]] = $data["kolicina"];
            } else {
                unset($_SESSION["cart"][$data["id"]]);
            }
        }
        break;
    case "purge_cart":
        unset($_SESSION["cart"]);
        break;
    case "confirm":
        
        try {
           
            DBorders::insert($_SESSION['user_id'], $_SESSION["sum"]);
            
            foreach($_SESSION["cart"]  as $key=>$value){
                try {
                    $id = DBorders::getId();
                    echo 
                    DBorders::insertOrderItem($id["MAX(id)"], $key, $value);
                } catch (Exception $e) {
                    
                    echo "Napaka pri poizvedbi: " . $e->getMessage();
                }
                $url = basename(__FILE__) . "?do=edit_order+";
            }
            ?>
        <script> alert("Naročilo oddano"); </script>
        
        <?php
        
        } catch (Exception $e) {
            echo "Napaka pri poizvedbi: " . $e->getMessage();
        }
        unset($_SESSION["cart"]);
        
        break;
    default:
        break;
}
if($data["do"] != "order_cart"):
    
?>
    <!DOCTYPE html>
    <html>
        <head>
            <link rel="stylesheet" href="static/styles.css">
            <meta charset="UTF-8" />
            <link rel="stylesheet" href="static/styles.css">
            <title>Trgovina</title>
        </head>
        <body>
        <div class="header">
            <img id="logo" src="static/images/logo.png">
        </div>
        <div class="header-links">
            <?php
            if (isset($_SESSION["type"])):
                echo '<a href="login.php">Nazaj na profil</a>';
                echo '<a style="float:right" href="logout.php">Odjava</a>';
            else:
                echo '<a href="registration.php">Registracija</a>';
                echo '<a href="login.php">Nazaj na login</a>';
              
            endif;
            ?>
         
            
        </div>
        <h1>Trgovina</h1>

        <div id="main">
            <?php foreach (DBitems::getAll() as $item): 
                if($item["active"]==1):
                ?>
                <div class="item">
                    <form action="<?= $url ?>" method="post">
                        <input type="hidden" name="do" value="add_into_cart" />
                        <input type="hidden" name="id" value="<?= $item["id"] ?>" />
                        <img src="<?= $item["image"] ?>">
                        <p><?= $item["name"] ?>: <?= $item["size"] ?></p>
                        <p><?= number_format($item["price"], 2) ?> EUR<br/>
                            <?php 
                            if(isset($_SESSION["type"])){
                            ?>
                            <button type="submit" class="button-grey">V košarico</button>
                            <?php } ?>
                    </form>
                </div>
            <?php 
            endif;
            endforeach; ?>
        </div>
        <?php 
        if(isset($_SESSION["type"])):
        ?>
            <div class="cart">
                <h3>Košarica</h3>

                <?php
                $kosara = isset($_SESSION["cart"]) ? $_SESSION["cart"] : [];

                if ($kosara):
                    $znesek = 0;

                    foreach ($kosara as $id => $kolicina):
                        $item = DBitems::get($id);
                        $znesek += $item["price"] * $kolicina;
                        ?>
                        <form action="<?= $url ?>" method="post">
                            <input type="hidden" name="do" value="update_cart" />
                            <input type="hidden" name="id" value="<?= $item["id"] ?>" />
                            <input type="number" name="kolicina" value="<?= $kolicina  ?>"
                               class="short_input" />
                            &times; <?=
                                (strlen($item["name"]) < 30) ?
                                $item["name"] :
                                substr($item["name"], 0, 26) . " ..."
                            ?> 
                            <button type="submit" class="button-small">Posodobi</button> 
                        </form>
                <?php endforeach; ?>

                <p>Skupaj: <b><?= number_format($znesek, 2) ?> EUR</b></p>

                <form action="<?= $url ?>" method="POST">
                    <input type="hidden" name="do" value="purge_cart" />
                    <button type="submit" class="button-blue">Izprazni košarico</button>
                </form>
                
                <form action="<?= $url ?>" method="POST">
                    <input type="hidden" name="do" value="order_cart" />
                    <button type="submit" class="button-blue">Na blagajno</button>
                </form>    
                
            <?php else: ?>
                Košara je prazna.                
            <?php endif; ?>
            
            </div>
            <?php endif; ?>    
        </body>
    </html>
<?php else: ?>  
<html>
<head>
        
        <link rel="stylesheet" href="static/styles.css">
        <meta charset="UTF-8" />
        <title>Pregled naročila</title>
</head>
    <body>
        
        <div class="center2">
            <h1>Pregled naročila</h1>
        <?php $sum = 0; ?>
            
            <table frame="box" class="order-table" rules="groups" cellpadding="7">
                <thead>
                    <tr>
                       <th> Ime produkta </th>
                       <th> model </th
                       ><th> Barva </th> 
                       <th> Velikost </th>
                       <th> Cena izdelka </th>
                       <th> Število izdelkov </th>
                       <th> Celotna cena </th>
                    </tr>
                </thead>
                
                <?php
                
                foreach($_SESSION["cart"]  as $key=>$value){
                    try {
                        $item= DBitems::get($key); // POIZVEDBA V PB
                    } catch (Exception $e) {
                        echo "Napaka pri poizvedbi: " . $e->getMessage();
                    }
                    $url = basename(__FILE__) . "?do=edit_order+";
                    $sum = $sum + ($item["price"] * $value);
                    $_SESSION["sum"] = $sum; 
  
                ?>
                    <tbody>
                        <tr>
                            <td  align="center"> <?php echo $item["name"] ?> </td>
                            <td  align="center"> <?php echo $item["shape"] ?> </td>
                            <td  align="center"> <?php echo $item["color"] ?> </td> 
                            <td  align="center"> <?php echo $item["size"] ?> </td> 
                            <td  align="center"> <?= number_format($item["price"], 2) ?> EUR </td>
                            <td  align="center"><?php echo $value ?> </td>
                            <td  align="center"><?= number_format($item["price"]*$value, 2) ?> EUR</td>
                        </tr>
                    </tbody>
                <?php  }  ?>
                <tfoot>
                    <tr>
                        <td><b>Skupaj:</b></td>
                        <td align="right"> <b><?= number_format($sum, 2) ?> EUR</b></td>
                    </tr>
                </tfoot>
            </table>
       
        </br>
        <div class="center">
            <form action="<?= $url ?>" method="POST">
                <input type="hidden" name="do" value="confirm" />
                <button type="submit" class="button-green" >Oddaj</button>
            </form> 
        </div>
        
         </div>
    </body>
</html>
<?php endif; ?>    
